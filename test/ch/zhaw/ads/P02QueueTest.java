/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.zhaw.ads;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author niclas
 */
public class P02QueueTest {
    
    public P02QueueTest() {
    }
    
    

    /**
     * Test of add method, of class P02Queue.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        Object o = new String("asdf");
        P02Queue instance = new P02Queue();
        assertTrue(instance.isEmpty());
        instance.add(o);
        assertFalse(instance.isEmpty());
    }

    /**
     * Test of remove method, of class P02Queue.
     */
    @Test
    public void testRemove() {
        System.out.println("remove");
        P02Queue instance = new P02Queue();
        Object o = new Integer(5);
        instance.add(o);
        Object q = instance.remove();
        assertTrue(instance.isEmpty());
        assertEquals(q.toString(), "5");
    }

    /**
     * Test of isEmpty method, of class P02Queue.
     */
    @Test
    public void testIsEmpty() {
        System.out.println("isEmpty");
        P02Queue instance = new P02Queue();
        assertTrue(instance.isEmpty());
        instance.add(new Integer(4));
        assertFalse(instance.isEmpty());
    }
    
}
