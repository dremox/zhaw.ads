/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.zhaw.ads;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author niclas
 */
public class P01StackTest {
    
    P01Stack testStack;
    
    public P01StackTest() {
    }
    
    @Before
    public void setUp() {
        this.testStack = new P01Stack();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of push method, of class P01Stack.
     */
    @Test
    public void testPush() {
        System.out.println("push");
        Object x = null;
        assertEquals(testStack.isEmpty(), true);
        testStack.push(x);
        assertEquals(testStack.isEmpty(), false);
    }

    /**
     * Test of pop method, of class P01Stack.
     */
    @Test
    public void testPop() {
        System.out.println("pop");
        assertEquals(testStack.isEmpty(), true);
        assertEquals(testStack.pop(), null);
        testStack.push("asdf");
        assertEquals(testStack.isEmpty(), false);
        assertEquals(testStack.pop(), "asdf");
        assertEquals(testStack.isEmpty(), true);
    }

    /**
     * Test of peek method, of class P01Stack.
     */
    @Test
    public void testPeek() {
        System.out.println("peek");
        assertEquals(testStack.isEmpty(), true);
        assertEquals(testStack.peek(), null);
        testStack.push("asdf");
        assertEquals(testStack.isEmpty(), false);
        assertEquals(testStack.peek(), "asdf");
        assertEquals(testStack.isEmpty(), false);
    }

    /**
     * Test of isFull method, of class P01Stack.
     */
    @Test
    public void testIsFull() {
        System.out.println("isFull");
        //this has to return false at any time
        assertEquals(testStack.isFull(), false);
    }

    /**
     * Test of isEmpty method, of class P01Stack.
     */
    @Test
    public void testIsEmpty() {
        System.out.println("isEmpty");
        assertEquals(testStack.isEmpty(), true);
        testStack.push("asdf");
        assertEquals(testStack.isEmpty(), false);
        testStack.push("asdf");
        assertEquals(testStack.isEmpty(), false);
        testStack.removeAll();
        assertEquals(testStack.isEmpty(), true);
    }

    /**
     * Test of removeAll method, of class P01Stack.
     */
    @Test
    public void testRemoveAll() {
        System.out.println("removeAll");
        assertEquals(testStack.isEmpty(), true);
        testStack.push("asdf");
        assertEquals(testStack.isEmpty(), false);
        testStack.push("asdf");
        assertEquals(testStack.isEmpty(), false);
        testStack.removeAll();
        assertEquals(testStack.isEmpty(), true);
    }
    
}
