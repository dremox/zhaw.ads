/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.zhaw.ads;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author niclas
 */
public class P02LinkedListTest {
    
    public P02LinkedListTest() {
    }

    /**
     * Test of get method, of class P02LinkedList.
     */
    @Test
    public void testGet() {
        System.out.println("get");
        P02LinkedList instance = new P02LinkedList();
        assertNull(instance.get(-1));
        Object o1 = "asdf";
        Object o2 = "qwer";
        Object o3 = "yxcv";
        instance.add(o1);
        instance.add(o2);
        instance.add(o3);
        P02LinkedListElement e = (P02LinkedListElement) instance.get(1);
        assertEquals(e.getElement(), o2);
    }

    /**
     * Test of add method, of class P02LinkedList.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        Object o = "String";
        P02LinkedList instance = new P02LinkedList();
        assertTrue(instance.isEmpty());
        assertTrue(instance.add(o));
        assertFalse(instance.isEmpty());
    }

    /**
     * Test of remove method, of class P02LinkedList.
     */
    @Test
    public void testRemove() {
        System.out.println("remove");
        P02LinkedList instance = new P02LinkedList();
        Object o1 = "asdf";
        Object o2 = "qwer";
        Object o3 = "yxcv";
        assertNull(instance.remove(0));
        assertNull(instance.remove(-1));
        instance.add(o1);
        instance.add(o2);
        instance.add(o3);
        P02LinkedListElement removed = (P02LinkedListElement) instance.remove(1);
        assertEquals(removed.getElement(), o2);
        
    }

    /**
     * Test of isEmpty method, of class P02LinkedList.
     */
    @Test
    public void testIsEmpty() {
        System.out.println("isEmpty");
        P02LinkedList instance = new P02LinkedList();
        Object o = "String";
        assertTrue(instance.isEmpty());
        assertTrue(instance.add(o));
        assertFalse(instance.isEmpty());
        P02LinkedListElement removed = (P02LinkedListElement) instance.remove(0);
        assertEquals(o, removed.getElement());
        assertTrue(instance.isEmpty());
    }
    
}
