/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.zhaw.ads;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author niclas
 */
public class P03RankingServerTest {
    
    public P03RankingServerTest() {
    }
    
    @Test
    public void sortCompetitorData(){
        System.out.println("Sort Competitors by Time");
        P03RankingServer server = new P03RankingServer();
        server.sortCompetitorData();
        //test which competitor startnr is at position 1
        assertEquals(3, server.getCompetitorAtPosition(0).getStartNr());
        
        //test last competitor
        assertEquals(4383, server.getCompetitorAtPosition(server.getListSize()-1).getStartNr());
        
    }
    
}
