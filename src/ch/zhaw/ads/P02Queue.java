/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.zhaw.ads;

import java.util.LinkedList;

/**
 *
 * @author niclas
 */
public class P02Queue {
    private LinkedList<Object> queue;
    
    public P02Queue(){
        queueInit();
    }
    
    public void add(Object o){
        this.queue.addLast(o);
    }
    
    public Object remove(){
        return this.queue.removeFirst();
    }
    
    public boolean isEmpty(){
        return (this.queue.peekFirst() == null);
    }
    
    private void queueInit(){
        this.queue = new LinkedList();
    }
}
