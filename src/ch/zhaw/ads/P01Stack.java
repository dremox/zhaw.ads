/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.zhaw.ads;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author niclas
 */
public class P01Stack{
    
    private List<Object> stack;
    
    public P01Stack(){
        stackInit();
    }
    
    public void push(Object x){
        if(!isFull()){
            this.stack.add(x);
        }
    }
    
    public Object pop(){
        if(!isEmpty()){
            return removeAtIndex(getLastIndexOfStack());
        }else{
            return null;
        }
    }
    
    public Object peek(){
        if(!isEmpty()){
            return getAtIndex(getLastIndexOfStack());
        }else{
            return null;
        }
    }
    
    private Object removeAtIndex(int index){
        return stack.remove(index);
    }
    
    private Object getAtIndex (int index){
        return stack.get(index);
    }
    
    private int getLastIndexOfStack(){
        return (this.stack.size() > 0) ? this.stack.size()-1 : -1;
    }
    
    public boolean isFull(){
        return false;
    }
    
    public boolean isEmpty(){
        return (getLastIndexOfStack() == -1);
    }
    
    public void removeAll(){
        stack = null;
        stackInit();
    }
    
    private void stackInit(){
        this.stack = new ArrayList<Object>();
    }
}
