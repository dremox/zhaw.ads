/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.zhaw.ads;

import java.util.Comparator;

/**
 *
 * @author niclas
 */
public class P03CompetitorCompare implements Comparator<P03Competitor>{

    @Override
    public int compare(P03Competitor o1, P03Competitor o2) {
        return o1.compareTo(o2);
    }
    
}
