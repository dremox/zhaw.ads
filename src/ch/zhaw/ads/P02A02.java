/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.zhaw.ads;


/**
 *
 * @author niclas
 */
public class P02A02 implements ch.zhaw.ads.CommandExecutor {
    P02Queue myQueue = new P02Queue();
    
    @Override
    public String execute(String command) throws Exception {
        String[] input = command.split("\\s+");
        
        if(input[0].equals("add")){
            add(input[1]);
            return "'" + input[1] + "' was added to list!";
        }else if(input[0].equals("remove")){
            return "'" + remove().toString() + "' was removed from list!";
        }else{
            return "Wrong action!";
        }
    }
    
    private void add(String task){
        myQueue.add(task);
    }
    
    private Object remove(){
        return myQueue.remove();
    }
}
