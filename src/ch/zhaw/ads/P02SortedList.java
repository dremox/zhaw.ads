/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.zhaw.ads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author niclas
 * @param <T> Object Type
 */
public class P02SortedList<T> implements Comparator<T>{
    private List<T> list;
    
    public P02SortedList(){
        listInit();
    }
    
    public int insert(Object o){
        int i = 0;
        int insertPos = 0;
        for(; i < size(); i++){
            Object currentElement = this.list.get(i);
            if(compare((T) currentElement, (T) o) < 0){
                insertPos = i;
            }
            if(compare((T) currentElement, (T) o) >= 0){
                insertPos = i;
                break;
            }
        }
        this.list.add(insertPos, (T) o);
        return insertPos;
    }
    
    public Object get(int index){
        return this.list.get(index);
    }
    
    public int size(){
        return this.list.size();
    }
    
    private void listInit(){
        this.list = new ArrayList<>();
    }
    
    public List<T> getList(){
        return this.list;
    }

    @Override
    public int compare(T o1, T o2) {
        return o1.toString().compareTo(o2.toString());
    }
}
