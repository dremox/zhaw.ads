/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.zhaw.ads;

import java.io.*;
import java.util.*;

/**
 *
 * @author niclas
 */
public class P03RankingServer {

    List<P03Competitor> competitors;

    public P03RankingServer() {
        this.competitors = new ArrayList<P03Competitor>();
        loadCompetitorData();
        
    }
    
    public P03Competitor getCompetitorAtPosition(int index){
        return this.competitors.get(index);
    }
    
    public int getListSize(){
        return this.competitors.size();
    }
    
    public void sortCompetitorData(){
        Collections.sort(this.competitors, new P03CompetitorCompare());
        for(int i = 0; i < getListSize(); i++){
            this.competitors.get(i).setRank(i+1);
        }
    }

    public void loadCompetitorData() {
        String csvFile = "RangZuerich2011.csv"; //File is located in class folder
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";
        try {
            br = new BufferedReader(new FileReader(csvFile));
            
            while ((line = br.readLine()) != null) {

                // use ; as separator
                String[] competitor = line.split(cvsSplitBy);
                
                competitors.add(new P03Competitor(Integer.parseInt(competitor[0]), competitor[1], Integer.parseInt(competitor[2]), competitor[3], competitor[4]));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
