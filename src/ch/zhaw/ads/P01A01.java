/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.zhaw.ads;

/**
 *
 * @author niclas
 */
public class P01A01 implements ch.zhaw.ads.CommandExecutor {

    @Override
    public String execute(String command) throws Exception {
        String[] input = command.split("\\+");
        
        int first = Integer.parseInt(input[0]);
        int second = Integer.parseInt(input[1]);
        int result = first+second;
        return Integer.toString(result);
    }
    
}
