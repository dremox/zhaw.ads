/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.zhaw.ads;


/**
 *
 * @author niclas
 */
public class P01A04 implements ch.zhaw.ads.CommandExecutor {

    @Override
    public String execute(String command) throws Exception {
        return Boolean.toString(checkBrackets(command));
    }
    
    public boolean checkBrackets(String str){
        P01Stack stack = new P01Stack();
        for(int i = 0; i < str.length(); i++){
            char currentChar = str.charAt(i);
            switch(currentChar){
                case '(':
                case '{':
                case '[':
                    stack.push(currentChar);
                    break;
                case ')':
                    if((char) stack.peek() == '('){
                        stack.pop();
                    }else{
                        continue;
                    }
                    break;
                case '}':
                    if((char) stack.peek() == '{'){
                        stack.pop();
                    }else{
                        continue;
                    }
                    break;
                case ']':
                    if((char) stack.peek() == '['){
                        stack.pop();
                    }else{
                        continue;
                    }
                    break;
                default:
                    continue;
            }
        }
        return stack.isEmpty();
    }
}
