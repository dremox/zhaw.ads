/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.zhaw.ads;


/**
 *
 * @author niclas
 */
public class P02LinkedListElement {
    private Object element;
    private P02LinkedListElement nextElement;
    
    public P02LinkedListElement(Object o){
        this.element = o;
        this.nextElement = null;
    }
    
    public P02LinkedListElement getNext(){
        return this.nextElement;
    }
    
    public void setNext(P02LinkedListElement next){
        this.nextElement = next;
    }
    
    public Object getElement(){
        return this.element;
    }
}
