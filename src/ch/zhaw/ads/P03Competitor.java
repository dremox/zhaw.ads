/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.zhaw.ads;
import java.util.*;
import java.text.*;
/**
 *
 * @author niclas
 */
public class P03Competitor implements Comparable<P03Competitor> {
    private String name;
	private String country;
	private long time;
	private int jg;
	private int startNr;
	private int rank;

	public P03Competitor(int startNr, String name, int jg, String country, String time) {
		this.startNr = startNr;
                this.name = name;
                this.jg = jg;
                this.country = country;
                try{
                    this.time = parseTime(time);
                }catch(Exception e){
                    System.err.println("Wrong Dateformat supplied!");
                }
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public long getTime() {
		return time;
	}

	public String getName() {
		return name;
	}
  
	public int getJg() {
		return jg;
	}
        
        public int getStartNr(){
            return startNr;
        }
  
	private static long parseTime(String s) throws ParseException {
		DateFormat sdf = new SimpleDateFormat("HH:mm:ss.S");
		Date date = sdf.parse(s);
		return date.getTime();
	}

	public String toString() {
		SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss.S");
		StringBuilder sb = new StringBuilder();
		sb.append(rank);sb.append(" ");
                sb.append(startNr);sb.append(" ");
		sb.append(name); sb.append(" ");
		sb.append(Integer.toString(jg)); sb.append(" ");
		sb.append(df.format(new Date(time)));
		return sb.toString();
	}

    @Override
    public int compareTo(P03Competitor o) {
        if(o.time > this.time){
            return -1;
        }
        if(o.time < this.time){
            return 1;
        }
        return 0;
    }
    
}
