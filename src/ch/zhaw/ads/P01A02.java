/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.zhaw.ads;

/**
 * Use a space to seperate Values
 * @author niclas
 */
public class P01A02 implements ch.zhaw.ads.CommandExecutor {

    @Override
    public String execute(String command) throws Exception {
        String[] input = command.split("\\s+");
        
        return Integer.toString(kgv(Integer.parseInt(input[0]), Integer.parseInt(input[1])));
        
    }
    
    private int kgv(int eins, int zwei){
        return (eins * zwei) / ggt(eins, zwei);
    }
    
    private int ggt(int a, int b) {
        if(a == b || b == 0) {
            return a;
        }else{
            return ggt(b, a%b);
        }
    }
    
}
