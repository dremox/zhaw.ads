/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.zhaw.ads;

import java.util.AbstractList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author niclas
 */
public class P02LinkedList extends AbstractList {
    private P02LinkedListElement listStart = null;
    
    @Override
    public Object get(int index) {
        P02LinkedListElement e = this.listStart;
        if(index < 0){
            return null;
        }
        for(int i = 0; i < index; i++){
            e = e.getNext();
        }
        return e;
    }
    
    @Override
    public boolean add(Object o){
        if(isEmpty()){
            listStart = new P02LinkedListElement(o);
            return true;
        }else{
            P02LinkedListElement newElement = new P02LinkedListElement(o);
            P02LinkedListElement e = this.listStart;
            while(e.getNext() != null){
                e = e.getNext();
            }
            e.setNext(newElement);
            return true;
        }
    }
    
    @Override
    public Object remove(int pos){
        if(this.listStart == null){
            return null;
        }
        if(pos == 0){
            P02LinkedListElement newHead = this.listStart.getNext();
            P02LinkedListElement oldHead = this.listStart;
            this.listStart = newHead;
            return oldHead;
        }
        if(pos < 0){
            return null;
        }
        P02LinkedListElement e = (P02LinkedListElement) get(pos);
        P02LinkedListElement prev = (P02LinkedListElement) get(pos-1);
        P02LinkedListElement newNext = e.getNext();
        prev.setNext(newNext);
        return e;
    }
   
    @Override
    public boolean isEmpty(){
        return (this.listStart == null);
    }

    @Override
    public int size() {
        throw new UnsupportedOperationException("Not supported."); 
    }

    @Override
    protected void removeRange(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException("Not supported."); 
    }

    @Override
    public int hashCode() {
        throw new UnsupportedOperationException("Not supported."); 
    }

    @Override
    public boolean equals(Object o) {
        throw new UnsupportedOperationException("Not supported."); 
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException("Not supported."); 
    }

    @Override
    public ListIterator listIterator(int index) {
        throw new UnsupportedOperationException("Not supported."); 
    }

    @Override
    public ListIterator listIterator() {
        throw new UnsupportedOperationException("Not supported."); 
    }

    @Override
    public Iterator iterator() {
        throw new UnsupportedOperationException("Not supported."); 
    }

    @Override
    public boolean addAll(int index, Collection c) {
        throw new UnsupportedOperationException("Not supported."); 
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Not supported."); 
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException("Not supported."); 
    }

    @Override
    public int indexOf(Object o) {
        throw new UnsupportedOperationException("Not supported."); 
    }

    @Override
    public void add(int index, Object element) {
        throw new UnsupportedOperationException("Not supported."); 
    }

    @Override
    public Object set(int index, Object element) {
        throw new UnsupportedOperationException("Not supported."); 
    }
    
}
